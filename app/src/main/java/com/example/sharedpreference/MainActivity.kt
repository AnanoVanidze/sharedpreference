package com.example.sharedpreference

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        sharedPreferences = getSharedPreferences("aboutUser", MODE_PRIVATE)

        val email = sharedPreferences.getString("email", "")
        val firstName = sharedPreferences.getString("firstName", "")
        val lastName = sharedPreferences.getString("lastName", "")
        val age = sharedPreferences.getInt("age", 0)
        val address = sharedPreferences.getString("address", "")

        emailET.setText(email)
        firstNameET.setText(firstName)
        lastNameET.setText(lastName)
        ageET.setText(age.toString())
        addressET.setText(address)
    }

    fun save(view: View) {

        val email = emailET.text.toString()
        val firstName = firstNameET.text.toString()
        val lastName = lastNameET.text.toString()
        val age = ageET.text.toString()
        val address = addressET.text.toString()

        if (email.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || age.
                isEmpty() || address.isEmpty()
        ) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
            if (email.isEmpty() && firstName.isEmpty() && lastName.isEmpty() && age.toString()
                    .isEmpty() && address.isEmpty())
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()




        } else {
            val edit = sharedPreferences.edit()
            edit.putString("email", email)
            edit.putString("firstName", firstName)
            edit.putString("lastName", lastName)
            edit.putInt("age", age.toInt())
            edit.putString("address", address)
            edit.apply()
        }


    }


}